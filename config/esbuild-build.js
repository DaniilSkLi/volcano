const esbuild = require('esbuild');
const path = require("path");

const mode = process.env.MODE || 'development';

const isProd = mode === 'production';
const isDev = !isProd;

const resolve = (...args) => {
  return path.resolve(__dirname, '..', ...args);
}

esbuild.build({
  platform: "node",
  outdir: resolve('dist'),
  entryPoints: [resolve('src', 'main.ts')],
  tsconfig: resolve('tsconfig.json'),
  entryNames: 'main.bundle',
  bundle: true,
  minify: isProd,
});